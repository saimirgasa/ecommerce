## About
This is the coding assignment

## Tests
Test are unfortunately not all passing

## How to use it
* Please run the `EcommerceApplication` main class
* Run `curl` commands to consume the API

## Documentation
After the application is running the API documentation can be reached here:
http://localhost:8080/swagger-ui/

The Swagger UI can be used for testing as well.
