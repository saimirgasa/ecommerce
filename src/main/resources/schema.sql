drop table if exists product;
drop table if exists ordering;

create table ordering
(
    id  bigint not null
        constraint ordering_pkey
            primary key,
    buyer_email      varchar(255),
    datatime_created timestamp,
    description      varchar(255)
);

create table product
(
    id  bigint not null
        constraint product_pkey
            primary key,
    date_created date,
    deleted      boolean,
    name         varchar(255),
    price        double precision,
    ordering_id  bigint,
    foreign key (ordering_id) references ordering(id) on delete cascade
);
