insert into ordering (id, buyer_email, datatime_created, description) values
    (8, 'saimir.gasa@gmail.com', '2008-11-11 13:23:44', 'This is the first order'),
    (9, 'saimir.gasa@gmail.com', '2009-11-11 13:23:44', 'This is the second order'),
    (10, 'saimir.gasa@gmail.com', '2020-11-11 13:23:44', 'This is the third order');

insert into product (id, name, date_created, deleted, price, ordering_id) values
    (1, 'iPhone', '2020-10-09', false, 1500, 8),
    (2, 'MacBook Pro', '2020-10-09', false, 3500, 9),
    (3, 'iPad', '2020-10-09', false, 600, 10),
    (4, 'mouse', '2020-10-09', false, 120, 8),
    (5, 'monitor', '2020-10-09', false, 800, 9),
    (6, 'ear pods', '2020-10-09', false, 220, 10),
    (7, 'charging cable', '2020-10-09', false, 45, 8);
