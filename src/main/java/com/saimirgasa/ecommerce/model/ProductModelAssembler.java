package com.saimirgasa.ecommerce.model;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.saimirgasa.ecommerce.domain.Product;
import com.saimirgasa.ecommerce.web.rest.ProductResource;

@Component
public class ProductModelAssembler implements RepresentationModelAssembler<Product, EntityModel<Product>> {

    @Override
    public EntityModel<Product> toModel(Product product) {
        return EntityModel.of(product,
            linkTo(methodOn(ProductResource.class).getProduct(product.getId())).withSelfRel(),
            linkTo(methodOn(ProductResource.class).getAllProducts()).withRel("products"));
    }
}
