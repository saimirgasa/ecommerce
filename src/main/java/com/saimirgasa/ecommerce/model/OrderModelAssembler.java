package com.saimirgasa.ecommerce.model;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.saimirgasa.ecommerce.domain.Order;
import com.saimirgasa.ecommerce.web.rest.OrderResource;

@Component
public class OrderModelAssembler implements RepresentationModelAssembler<Order, EntityModel<Order>> {
    @Override
    public EntityModel<Order> toModel(Order order) {
        return EntityModel.of(order,
            linkTo(methodOn(OrderResource.class).getOrder(order.getId())).withSelfRel(),
            linkTo(methodOn(OrderResource.class).getAllOrders()).withRel("orders"));
    }
}
