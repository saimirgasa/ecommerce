package com.saimirgasa.ecommerce.web.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.saimirgasa.ecommerce.domain.Order;
import com.saimirgasa.ecommerce.domain.Product;
import com.saimirgasa.ecommerce.model.OrderModelAssembler;
import com.saimirgasa.ecommerce.repository.OrderRepository;
import com.saimirgasa.ecommerce.web.rest.errors.OrderNotFoundException;

/**
 * REST controller for managing {@link com.saimirgasa.ecommerce.domain.Order}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class OrderResource {

    private final Logger log = LoggerFactory.getLogger(OrderResource.class);

    private static final String ENTITY_NAME = "order";

    private final OrderRepository orderRepository;

    private final OrderModelAssembler orderModelAssembler;

    public OrderResource(OrderRepository orderRepository, OrderModelAssembler orderModelAssembler) {
        this.orderRepository = orderRepository;
        this.orderModelAssembler = orderModelAssembler;
    }

    /**
     * {@code POST  /orders} : Create a new order.
     *
     * @param order the order to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new order, or with status {@code 400 (Bad Request)} if the order has already an ID.
     */
    @PostMapping("/orders")
    public ResponseEntity<EntityModel<Order>> createOrder(@RequestBody Order order) {
        log.debug("REST request to save Order : {}", order);
        EntityModel<Order> entityModel = orderModelAssembler.toModel(order);

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(entityModel);
    }

    /**
     * {@code PUT  /orders} : Updates an existing order.
     *
     * @param order the order to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated order,
     * or with status {@code 400 (Bad Request)} if the order is not valid,
     * or with status {@code 500 (Internal Server Error)} if the order couldn't be updated.
     */
    @PutMapping("/orders")
    public ResponseEntity<EntityModel<Order>> updateOrder(@RequestBody Order order, @PathVariable Long id) {
        log.debug("REST request to update Order : {}", order);
        Order updatedOrder = orderRepository.findById(id)
            .map(o -> {
                o.setBuyerEmail(order.getBuyerEmail());
                o.setDatetimeCreated(order.getDatetimeCreated());
                o.setDescription(order.getDescription());
                o.setProducts(order.getProducts());
                return orderRepository.save(o);
            })
            .orElseGet(() -> {
                order.setId(id);
                return orderRepository.save(order);
            });

        EntityModel<Order> entityModel = orderModelAssembler.toModel(updatedOrder);

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(entityModel);
    }

    /**
     * {@code GET  /orders} : get all the orders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orders in body.
     */
    @GetMapping("/orders")
    public CollectionModel<EntityModel<Order>> getAllOrders() {
        log.debug("REST request to get a page of Orders");

        List<EntityModel<Order>> orders = orderRepository.findAll().stream()
            .map(order -> EntityModel.of(order,
                linkTo(methodOn(OrderResource.class).getAllOrders()).withRel("orders")))
            .collect(Collectors.toList());
        return CollectionModel.of(orders,
            linkTo(methodOn(OrderResource.class).getAllOrders()).withSelfRel());
    }

    /**
     * {@code GET  /orders/:id} : get the "id" order.
     *
     * @param id the id of the product to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the order, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/orders/{id}")
    public EntityModel<Order> getOrder(@PathVariable Long id) {
        log.debug("REST request to get Order : {}", id);
        Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
        return orderModelAssembler.toModel(order);
    }

    /**
     * {@code DELETE  /orders/:id} : delete the "id" order.
     *
     * @param id the id of the order to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable Long id) {
        log.debug("REST request to delete Order : {}", id);
        orderRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/getordertotal/{id}")
    public ResponseEntity<Double> getOrderTotal(@PathVariable Long id) {
        log.debug("REST request to get Order total: {}", id);
        Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
        double totalAmount = 0;

        for (Product product : order.getProducts()) {
            totalAmount += product.getPrice();
        }
        return ResponseEntity.ok().body(totalAmount);
    }

    @GetMapping("/ordersbydaterange/{from}/{to}")
    public CollectionModel<EntityModel<Order>> getOrdersByDateRange(
                                        @PathVariable String from,
                                        @PathVariable String to)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime start = LocalDateTime.parse(from, formatter);
        LocalDateTime end = LocalDateTime.parse(to, formatter);

        List<Order> orders = orderRepository.findAll().stream()
            .filter(order -> order.getDatetimeCreated().isAfter(start) && order.getDatetimeCreated().isBefore(end))
            .collect(Collectors.toList());

        List<EntityModel<Order>> entityModelOrders = orders.stream()
            .map(orderModelAssembler::toModel)
            .collect(Collectors.toList());

        return CollectionModel.of(entityModelOrders,
            linkTo(methodOn(OrderResource.class).getAllOrders()).withSelfRel());
    }
}
