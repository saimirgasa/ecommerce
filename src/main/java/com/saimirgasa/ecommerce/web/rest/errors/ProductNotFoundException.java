package com.saimirgasa.ecommerce.web.rest.errors;

public class ProductNotFoundException extends RuntimeException {
    public ProductNotFoundException(Long id) {
        super("Could not find Product with id: " + id);
    }
}
