package com.saimirgasa.ecommerce.web.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.saimirgasa.ecommerce.domain.Product;
import com.saimirgasa.ecommerce.model.ProductModelAssembler;
import com.saimirgasa.ecommerce.repository.ProductRepository;
import com.saimirgasa.ecommerce.web.rest.errors.ProductNotFoundException;

/**
 * REST controller for managing {@link com.saimirgasa.ecommerce.domain.Product}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProductResource {

    private final Logger log = LoggerFactory.getLogger(ProductResource.class);

    private static final String ENTITY_NAME = "product";

    private final ProductRepository productRepository;

    private final ProductModelAssembler productModelAssembler;

    public ProductResource(ProductRepository productRepository, ProductModelAssembler productModelAssembler) {
        this.productRepository = productRepository;
        this.productModelAssembler = productModelAssembler;
    }

    /**
     * {@code POST  /products} : Create a new product.
     *
     * @param product the product to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new product, or with status {@code 400 (Bad Request)} if the product has already an ID.
     */
    @PostMapping("/products")
    public ResponseEntity<EntityModel<Product>> createProduct(@RequestBody Product product) {
        log.debug("REST request to save Product : {}", product);
        EntityModel<Product> entityModel = productModelAssembler.toModel(product);

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(entityModel);
    }

    /**
     * {@code PUT  /products} : Updates an existing product.
     *
     * @param product the product to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated product,
     * or with status {@code 400 (Bad Request)} if the product is not valid,
     * or with status {@code 500 (Internal Server Error)} if the product couldn't be updated.
     */
    @PutMapping("/products")
    public ResponseEntity<EntityModel<Product>> updateProduct(@RequestBody Product product) {
        log.debug("REST request to update Product : {}", product);
        Product updatedProduct = productRepository.findById(product.getId())
            .map(p -> {
                p.setName(product.getName());
                p.setPrice(product.getPrice());
                p.setDateCreated(product.getDateCreated());
                p.setOrder(product.getOrder());
                return productRepository.save(p);
            })
            .orElseGet(() -> productRepository.save(product));

        EntityModel<Product> entityModel = productModelAssembler.toModel(updatedProduct);

        return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(entityModel);
    }

    /**
     * {@code GET  /products} : get all the products.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of products in body.
     */
    @GetMapping("/products")
    public CollectionModel<EntityModel<Product>> getAllProducts() {
        log.debug("REST request to get a page of Products");
        List<Product> products = productRepository.findAll().stream()
            .filter(product -> !product.isDeleted())
            .collect(Collectors.toList());

        List<EntityModel<Product>> entityModelProducts = products.stream()
            .map(productModelAssembler::toModel)
            .collect(Collectors.toList());
        return CollectionModel.of(entityModelProducts,
            linkTo(methodOn(ProductResource.class).getAllProducts()).withSelfRel());
    }

    /**
     * {@code GET  /products/:id} : get the "id" product.
     *
     * @param id the id of the product to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the product, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/products/{id}")
    public EntityModel<Product> getProduct(@PathVariable Long id) {
        log.debug("REST request to get Product : {}", id);
        Product product = productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
        return productModelAssembler.toModel(product);
    }

    /**
     * {@code DELETE  /products/:id} : delete the "id" product.
     *
     * @param id the id of the product to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        log.debug("REST request to delete Product : {}", id);
        productRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }
}
