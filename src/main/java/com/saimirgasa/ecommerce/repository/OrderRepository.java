package com.saimirgasa.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.saimirgasa.ecommerce.domain.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
